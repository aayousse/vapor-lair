class AdjustmentsController < ApplicationController
  before_action :set_adjustment, only: [:show, :edit, :update, :destroy], except: :edit_discount
  before_action :must_be_manager

  # GET /adjustments
  # GET /adjustments.json
  def index
    @adjustments = Adjustment.all
  end

  # GET /adjustments/1
  # GET /adjustments/1.json
  def show
  end

  # GET /adjustments/new
  def new
    @customer = Customer.find_by_id(params[:customer_id])
    @customer && @adjustment = @customer.adjustments.build
    @adjustment ||= Adjustment.new
    @adjustment.employee = current_user
  end

  # GET /adjustments/1/edit
  def edit
  end

  # POST /adjustments
  # POST /adjustments.json
  def create
    @adjustment = Adjustment.new(adjustment_params)

    @customer = Customer.find_by_id(params[:customer][:id])
    new_current_points = params[:customer][:current_points].to_i

    if new_current_points < 0
      flash.now[:error] = "Points cannot be negative"
      render :new; return
    end

    delta = new_current_points - @customer.current_points
    new_lifetime_points = @customer.lifetime_points + delta
    @adjustment.change = delta

    if not @customer.update(current_points: new_current_points, lifetime_points: new_lifetime_points)
      flash.now[:error] = "Customer did not update."
      render :new; return
    end 

    respond_to do |format|
      if @adjustment.save
        format.html { redirect_to @customer, notice: 'Adjustment was successfully created.' }
        format.json { render :show, status: :created, location: @adjustment }
      else
        format.html { render :new }
        format.json { render json: @adjustment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /adjustments/1
  # PATCH/PUT /adjustments/1.json
  def update
    respond_to do |format|
      if @adjustment.update(adjustment_params)
        format.html { redirect_to @adjustment, notice: 'Adjustment was successfully updated.' }
        format.json { render :show, status: :ok, location: @adjustment }
      else
        format.html { render :edit }
        format.json { render json: @adjustment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /adjustments/1
  # DELETE /adjustments/1.json
  def destroy
    @adjustment.destroy
    respond_to do |format|
      format.html { redirect_to adjustments_url, notice: 'Adjustment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_adjustment
      @adjustment = Adjustment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def adjustment_params
      params.require(:adjustment).permit(:customer_id, :employee_id, :reason)
    end
end
