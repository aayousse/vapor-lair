class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper

  before_action :signed_in_status

  private

  	def signed_in_status
  		if signed_in?
  			@signed_in_status = "Signed in as #{current_user}"
  			if signed_in_customer?
  				@signed_in_status += " (Customer)"
  			elsif signed_in_associate?
  				@signed_in_status += " (Associate)"
  			elsif signed_in_manager?
  				@signed_in_status += " (Manager)"
  			end
  		else
  			@signed_in_status = "Not signed in"
  		end
  	end
end
