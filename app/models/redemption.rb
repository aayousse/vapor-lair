class Redemption < ActiveRecord::Base
  belongs_to :customer
  belongs_to :employee

  # Validations
  validates :customer_id, presence: true
  validates :employee_id, presence: true
  validates :date, presence: true
  validates :amount, presence: true

  @@discounts = [
  	10,
  	20,
  	05
  ]

  @@discount = 10

  def self.discounts
  	@@discounts
  end

  def self.discount
  	@@discount
  end

  def self.discount=(new_discount)
    @@discount = new_discount
  end

  def self.report(redemptions, options = {})
    CSV.generate(options) do |csv|
      csv << ["Customer First Name", "Customer Last Name", "Customer Email",
        "Points Redeemed", "Employee Last Name", "Date"]
      redemptions.each do |redemption|
        csv << [redemption.customer.fname, redemption.customer.lname,
          redemption.customer.email, redemption.amount,
          redemption.employee.lname, redemption.date.strftime("%m/%d/%Y")]
      end
    end
  end
end
