class Purchase < ActiveRecord::Base
  belongs_to :customer
  belongs_to :employee

  # Validations
  validates :customer_id, presence: true
  validates :employee_id, presence: true
  validates :date, presence: true
  validates :amount, presence: true
  validates :square_ref, presence: true

  def self.report(purchases, options = {})
  	CSV.generate(options) do |csv|
  		csv << ["Customer First Name", "Customer Last Name", "Customer Email",
  			"Purchase Amount", "Employee Last Name", "Date"]
  		purchases.each do |purchase|
  			csv << [purchase.customer.fname, purchase.customer.lname,
  				purchase.customer.email, "$%.2f" % purchase.amount,
  				purchase.employee.lname, purchase.date.strftime("%m/%d/%Y")]
  		end
  	end
  end
end
