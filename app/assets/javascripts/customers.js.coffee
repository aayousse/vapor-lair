# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
	$('#customer_phone, #customer_phone2').mask "(999) 999-9999"
	$('#customer_dob').mask "99/99/9999"