# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
	updatePoints = ->
		points = Math.round($(this).val() * 0.92)
		$('#purchase_points').html(points)

	$('#purchase_amount').keyup(updatePoints)
	$('#purchase_amount').change(updatePoints)
