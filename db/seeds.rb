# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Customer.create(
	{
		fname: "John",
		lname: "Doe",
		email: "john@example.com",
		password: "password"
	})

Employee.create([
	{
		fname: "Jane",
		lname: "Doe",
		email: "jane@example.com",
		address: "5 Drury Lane",
		city: "Houston",
		state: "TX",
		zip: "11111",
		password: "1234",
		manager: false
	},

	{
		fname: "Bruce",
		lname: "Springsteen",
		email: "theboss@example.com",
		address: "5 Drury Lane",
		city: "Houston",
		state: "TX",
		zip: "11111",
		password: "abcd",
		manager: true
	}])
