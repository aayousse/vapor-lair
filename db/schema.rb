# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140715045631) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "adjustments", force: true do |t|
    t.integer  "customer_id"
    t.integer  "employee_id"
    t.string   "reason"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "change"
  end

  add_index "adjustments", ["customer_id"], name: "index_adjustments_on_customer_id", using: :btree
  add_index "adjustments", ["employee_id"], name: "index_adjustments_on_employee_id", using: :btree

  create_table "customers", force: true do |t|
    t.string   "fname"
    t.string   "lname"
    t.string   "email"
    t.string   "email2"
    t.string   "email3"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.integer  "zip"
    t.string   "twitter"
    t.integer  "dlnumber"
    t.integer  "lifetime_points"
    t.integer  "current_points"
    t.string   "password"
    t.date     "pass_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "phone"
    t.string   "phone2"
    t.string   "dob"
  end

  create_table "employees", force: true do |t|
    t.string   "fname"
    t.string   "lname"
    t.string   "email"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.integer  "zip"
    t.string   "password"
    t.date     "pass_date"
    t.boolean  "manager"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "purchases", force: true do |t|
    t.integer  "customer_id"
    t.integer  "employee_id"
    t.date     "date"
    t.float    "amount"
    t.string   "square_ref"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "purchases", ["customer_id"], name: "index_purchases_on_customer_id", using: :btree
  add_index "purchases", ["employee_id"], name: "index_purchases_on_employee_id", using: :btree

  create_table "redemptions", force: true do |t|
    t.integer  "customer_id"
    t.date     "date"
    t.integer  "amount"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "employee_id"
  end

  add_index "redemptions", ["customer_id"], name: "index_redemptions_on_customer_id", using: :btree
  add_index "redemptions", ["employee_id"], name: "index_redemptions_on_employee_id", using: :btree

end
