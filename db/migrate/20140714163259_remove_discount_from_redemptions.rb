class RemoveDiscountFromRedemptions < ActiveRecord::Migration
  def change
    remove_column :redemptions, :discount, :float
  end
end
