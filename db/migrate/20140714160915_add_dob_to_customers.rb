class AddDobToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :dob, :integer
  end
end
