class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :fname
      t.string :lname
      t.string :email
      t.string :email2
      t.string :email3
      t.integer :phone
      t.integer :phone2
      t.string :address
      t.string :city
      t.string :state
      t.integer :zip
      t.string :twitter
      t.date :dob
      t.integer :dlnumber
      t.integer :lifetime_points
      t.integer :current_points
      t.string :password
      t.date :pass_date

      t.timestamps
    end
  end
end
