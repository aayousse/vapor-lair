class RemovePhoneAndDobFromCustomers < ActiveRecord::Migration
  def change
    remove_column :customers, :phone2, :integer
    remove_column :customers, :dob, :integer
  end
end
