class CreateRedemptions < ActiveRecord::Migration
  def change
    create_table :redemptions do |t|
      t.references :customer, index: true
      t.date :date
      t.integer :amount
      t.float :discount

      t.timestamps
    end
  end
end
