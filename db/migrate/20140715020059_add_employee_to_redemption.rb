class AddEmployeeToRedemption < ActiveRecord::Migration
  def change
    add_reference :redemptions, :employee, index: true
  end
end
